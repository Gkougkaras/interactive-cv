$(document).ready(function(){

    $(window).scroll(function(event){

        var y= $(this).scrollTop();

        if (y>=550){
            $(".projectImg").addClass("animate");
        }
        
        else{
            $(".projectImg").removeClass("animate");
        }

        if (y>=3200){
            $("#python").addClass("python-animate");
            $("#java").addClass("java-animate");
            $("#node").addClass("node-animate");
            $("#html5").addClass("html5-animate");
            $("#css3").addClass("css3-animate");
            $("#javascript").addClass("javascript-animate");
            $("#bootstrap").addClass("bootstrap-animate");
            $("#jquery").addClass("jquery-animate");
            $("#react").addClass("react-animate");
            $("#mysql").addClass("mysql-animate");
            $("#mongoDB").addClass("mongoDB-animate");
        }
        
        else{
            $("#python").removeClass("python-animate");
            $("#java").removeClass("java-animate");
            $("#node").removeClass("node-animate");
            $("#html5").removeClass("html5-animate");
            $("#css3").removeClass("css3-animate");
            $("#javascript").removeClass("javascript-animate");
            $("#bootstrap").removeClass("bootstrap-animate");
            $("#jquery").removeClass("jquery-animate");
            $("#react").removeClass("react-animate");
            $("#mysql").removeClass("mysql-animate");
            $("#mongoDB").removeClass("mongoDB-animate");
        }
    });
});

//Adding navbar links click triggers

var homeNav = document.getElementById("homeNav");
var projectsNav = document.getElementById("projectsNav");
var aboutNav = document.getElementById("aboutNav");
var footerNav =document.getElementById("footerNav");

homeNav.addEventListener("click",function(){
    window.scrollTo(0, 0);
});

projectsNav.addEventListener("click",function(){
    window.scrollTo(0,1200);
});
aboutNav.addEventListener("click",function(){
    window.scrollTo(0, 2800);
});

footerNav.addEventListener("click",function(){
    window.scrollTo(0,4400);
});

//Go to top button

var goToTop = document.querySelector(".fa-angle-double-up");

    goToTop.addEventListener("click",function(){
        window.scrollTo(0, 0);
    });

    