const express = require("express");
const app = express();

app.set("view engine", "ejs");
app.use(express.static(__dirname +"/public"));

//Index Route

app.get("/", function(req, res){
    res.render("index");
});


let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}
app.listen(port);